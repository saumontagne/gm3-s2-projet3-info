import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

public class TestSerialisation {

    public static void main(String args[]) {

        System.out.println("Sérialisation classique");

        // première mesure de temps
        long tempsDebut = System.currentTimeMillis();

        Personne personneAEcrire = new Personne("Jean Dupont", 25, "3 rue de Paris");
        System.out.println("Objet initial : " + personneAEcrire);

        // sérialisation
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("fichierobjet"));
            objectOutputStream.writeObject(personneAEcrire);
            objectOutputStream.flush(); // permet de sauvegarder les modifications faites au fichier
            objectOutputStream.close();
        } catch (IOException e) {
            System.err.println("Une erreur a eu lieu : ");
            e.printStackTrace();
        }

        // désérialisation
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("fichierobjet"));
            Personne personneALire = (Personne) objectInputStream.readObject();
            System.out.println("Objet lu : " + personneALire);
            objectInputStream.close();
        } catch (ClassNotFoundException | IOException e) {
            System.err.println("Une erreur a eu lieu : ");
            e.printStackTrace();
        }

        long tempsFin = System.currentTimeMillis();
        long tempsEcoule = tempsFin - tempsDebut;
        System.out.println("Temps écoulé : " + tempsEcoule);


        System.out.println("\nSérialisation avec Externalizable");

        // première mesure de temps
        tempsDebut = System.currentTimeMillis();

        PersonneExternalizable personneExternalizableAEcrire = new PersonneExternalizable("Jean Dupont", 25, "3 rue de Paris");
        System.out.println("Objet initial : " + personneExternalizableAEcrire);

        // sérialisation
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("fichierobjetexternalizable"));
            objectOutputStream.writeObject(personneExternalizableAEcrire);
            objectOutputStream.flush(); // permet de sauvegarder les modifications faites au fichier
            objectOutputStream.close();
        } catch (IOException e) {
            System.err.println("Une erreur a eu lieu : ");
            e.printStackTrace();
        }

        // désérialisation
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("fichierobjetexternalizable"));
            PersonneExternalizable personneExternalizableALire = (PersonneExternalizable) objectInputStream.readObject();
            System.out.println("Objet lu : " + personneExternalizableALire);
            objectInputStream.close();
        } catch (ClassNotFoundException | IOException e) {
            System.err.println("Une erreur a eu lieu : ");
            e.printStackTrace();
        }

        tempsFin = System.currentTimeMillis();
        tempsEcoule = tempsFin - tempsDebut;
        System.out.println("Temps écoulé : " + tempsEcoule);


        System.out.println("\nSérialisation par objet proxy");

        // première mesure de temps
        tempsDebut = System.currentTimeMillis();

        PersonneARemplacer personneARemplacer = new PersonneARemplacer("Jean Dupont", 25, "3 rue de Paris");
        System.out.println("Objet initial : " + personneARemplacer);

        // sérialisation
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("fichierobjetproxy"));
            objectOutputStream.writeObject(personneARemplacer);
            objectOutputStream.flush(); // permet de sauvegarder les modifications faites au fichier
            objectOutputStream.close();
        } catch (IOException e) {
            System.err.println("Une erreur a eu lieu : ");
            e.printStackTrace();
        }

        // désérialisation
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("fichierobjetproxy"));
            PersonneARemplacer personneARemplacerALire = (PersonneARemplacer) objectInputStream.readObject();
            System.out.println("Objet lu : " + personneARemplacerALire);
            objectInputStream.close();
        } catch (ClassNotFoundException | IOException e) {
            System.err.println("Une erreur a eu lieu : ");
            e.printStackTrace();
        }

        tempsFin = System.currentTimeMillis();
        tempsEcoule = tempsFin - tempsDebut;
        System.out.println("Temps écoulé : " + tempsEcoule);
    }
}

import java.io.Serializable;
import java.io.ObjectStreamException;


public class PersonneARemplacer extends Personne implements Serializable {

    public PersonneARemplacer(String nom, Integer age, String adresse) {
        super(nom, age, adresse);
    }

    // méthode standard appelée pour la sérialisation avec proxy
    private Object writeReplace() throws ObjectStreamException {

        // cette méthode retourne l'objet proxy, qui va être sérialisé à la place de celui-ci
        return  new PersonneProxy(this) ;
    }
}

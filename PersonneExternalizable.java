import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.IOException;

public class PersonneExternalizable implements Externalizable {
    // attributs
    private String nom;
    private Integer age;
    private String adresse;

    // constructeur par défaut
    public PersonneExternalizable() {
        System.out.println("Constructeur par défaut appelé");
    }

    // constructeur
    public PersonneExternalizable(String nom, Integer age, String adresse) {
        this.nom = nom;
        this.age = age;
        this.adresse = adresse;
    }



    // accesseurs
    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getAge() {
        return this.age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    // méthodes
    public String toString() {
        return "Nom : " + (this.nom == null ? "N/A" : this.nom) + ", " +
        (this.age == null ? "N/A" : this.age) + " ans, adresse : "
        + (this.adresse == null ? "N/A" : this.adresse);
    }

    // redéfinition de writeExternal et readExternal nécessaire
    // l'adresse n'est pas sérialisée
    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeUTF(nom);
        objectOutput.writeInt(age);
        System.out.println("SÉRIALISATION !");
    }
    public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
        this.nom = objectInput.readUTF();
        this.age = objectInput.readInt();
        System.out.println("DÉSÉRIALISATION !");
    }


}

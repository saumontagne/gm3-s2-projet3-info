import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.NoSuchElementException;
import java.lang.IllegalStateException;
import java.lang.SecurityException;

public class TestEntreeSortie {

  public static void main (String args[]){

    Scanner sc1 = new Scanner(System.in);
    // création d'un scanner qui lit dans le flux d'entrée standard
    int unEntier;
    double unDouble;
    String uneChaine, chaineLue;
    //int caractere;
    DataOutputStream dos;
    try{
      dos = new DataOutputStream(new FileOutputStream("fichierbinaire"));
      // FileOutputStream : ouvre le fichier binaire "fichierbinaire" en écriture
      // DataOutputStream : convertit les objets en octets

      System.out.print("Entrez un entier : ");
      unEntier = sc1.nextInt(); // récupère l'entier entré sur le terminal
      System.out.print("Entrez un réel : ");
      unDouble = sc1.nextDouble(); // récupère le réel entré sur le terminal
      // il faut utiliser les , et pas . (ex : 12,1)
      dos.writeInt(unEntier);   // écrit l'entier dans le fichierbinaire
      dos.writeDouble(unDouble);  // écrit le double dans le fichierbinaire
      dos.close();  // fermeture du fichier fichierbinaire
    }
    catch(IOException | NoSuchElementException | IllegalStateException e){
      System.err.println("Une erreur a eu lieu : ");
      e.printStackTrace();
    }

    try{
      FileWriter fileWriter = new FileWriter("fichiertexte");
      // FileWriter : ouvre le fichier texte en écriture
      System.out.print("Entrez une chaine de caractères : ");
      sc1.nextLine();
      uneChaine = sc1.nextLine();
      fileWriter.write(uneChaine);
      fileWriter.close();
    }
    catch(IOException | NoSuchElementException | IllegalStateException e){
      System.err.println("Une erreur a eu lieu : ");
      e.printStackTrace();
    }

    System.out.println(); // fermeture du fichier fichiertexte

    try{
      DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream("fichierbinaire")));
      // FileInputStream : ouvre le fichier binaire en lecture
      // BufferedInputStream : permet de récupérer plusieurs octets à la fois
      // DataInputStream : convertit les octets en objets

      System.out.println("Le fichier contient :");
      System.out.println("l'entier : " + dis.readInt());  // Affiche l'entier lu dans le fichier
      System.out.println("le double : " + dis.readDouble());  // Affiche le double lu dans le fichier
      dis.close();  // fermeture du fichier fichierbinaire
    }
    catch(SecurityException | IOException e){
      System.err.println("Une erreur a eu lieu : ");
      e.printStackTrace();
    }

    try{
      BufferedReader fileReader = new BufferedReader(new FileReader("fichiertexte"));
      // FileReader : ouvre le fichier texte en lecture
      // BufferedReader : permet de lire plusieurs caractères à la fois

      System.out.println("la chaine : " + fileReader.readLine());
      // Affiche la chaine de caractères lue dans le fichier

      fileReader.close(); // fermeture du fichier fichiertexte
    }
    catch(IOException e){
      System.err.println("Une erreur a eu lieu : ");
      e.printStackTrace();
    }

    System.out.println();

    try{
      Scanner sc2 = new Scanner(args[0]).useDelimiter("[\\-]"); // args[0]="73-37,0"
      // création d'un scanner qui lit dans les arguments passés lors de l'appel du programme
      int nbentier2 = sc2.nextInt();  // lit un entier dans les arguments
      double nbdouble2 = sc2.nextDouble();  // lit un double dans les arguments
      System.out.println("Parmi les arguments, on a :");
      System.out.printf("un entier : %d, un double : %.2f \n", nbentier2, nbdouble2);
    }
    catch(NoSuchElementException | IllegalStateException e){
      System.err.println("Une erreur a eu lieu : ");
      e.printStackTrace();
    }
  }
}

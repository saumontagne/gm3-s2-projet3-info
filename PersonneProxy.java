import java.io.Serializable;
import java.io.ObjectStreamException;
import java.util.StringTokenizer;

public class PersonneProxy implements Serializable {

    // un seul champ à sérialiser
    private String data;

    // constructeur utilitaire pour construire un proxy
    public PersonneProxy(PersonneARemplacer personne) {
        // l'unique champ concatène le nom et l'âge d'une personne
        this.data = personne.getNom() +  ":" + personne.getAge();
    }

    // méthode appelée pour la désérialisation : lorsque cette méthode est appelée, le champ data a été désérialisé
    private Object readResolve() throws ObjectStreamException {
        StringTokenizer st =  new StringTokenizer(this.data,  ":");
        String nom = st.nextToken();
        Integer age = Integer.parseInt(st.nextToken());

        PersonneARemplacer personne =  new PersonneARemplacer(nom, age, null);
        return personne;
    }


}

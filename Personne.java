import java.io.Serializable;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

public class Personne implements Serializable {
    // attributs
    private String nom;
    private Integer age;

    // attribut non sérialisé
    private transient String adresse;

    // constructeur
    public Personne(String nom, Integer age, String adresse) {
        this.nom = nom;
        this.age = age;
        this.adresse = adresse;
    }

    // accesseurs
    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getAge() {
        return this.age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    // méthodes
    public String toString() {
        return "Nom : " + (this.nom == null ? "N/A" : this.nom) + ", " +
        (this.age == null ? "N/A" : this.age) + " ans, adresse : "
        + (this.adresse == null ? "N/A" : this.adresse);
    }

    // personnalisation du comportement de la sérialisation (redéfinition de writeObject et readObject)
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject(); // pour le comportement par défaut
            System.out.println("SÉRIALISATION !");
    }
    private void readObject(ObjectInputStream objectInputStream) throws IOException {
        try {
            objectInputStream.defaultReadObject(); // pour le comportement par défaut
            System.out.println("DÉSÉRIALISATION !");
        } catch (ClassNotFoundException e) {
            System.out.println("Une erreur a eu lieu : " + e.getMessage());
        }
    }


}

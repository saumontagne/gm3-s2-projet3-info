\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{enumitem}
\usepackage[french]{babel}
\usepackage[left=3cm,right=3cm,top=3cm,bottom=3cm]{geometry}
\usepackage[colorlinks = true,
			linkcolor = blue]{hyperref}
\usepackage{graphicx, float, amssymb, minted}

%opening
\title{~\\~\\~\\~\\~\\~\\~\\~\\ Projet informatique \\ \textit{Présentation des Entrées/Sorties et sérialisation en Java} \\~\\~\\~\\~\\~\\}
\author{Marie \textsc{Bochet} et Robin \textsc{Langlois}}

\begin{document}

\maketitle

%\begin{abstract}
%\end{abstract}

\newpage

\tableofcontents
%\listoffigures

\newpage

\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

Le but de ce projet est d'étudier les Entrées/Sorties et la sérialisation en Java. Ces outils permettent d'échanger avec l'environnement du programme, ou de stocker des informations dans la durée. Nous allons donc présenter le fonctionnement de ces outils dans la globalité, et réaliser des programmes illustrant leurs différentes utilisations. Nous commenterons alors ce code avant de montrer des exemples d'utilisation de ces derniers.

\newpage
\section{Les Entrées/Sorties}

\subsection{Présentation générale}

Afin de pouvoir coder correctement un programme, il est essentiel de pouvoir gérer les Entrées/Sorties, qui permettent de communiquer avec l'utilisateur ou des données extérieures au programme. Il faut en effet pouvoir lire (avec les Entrées) et écrire (avec les Sorties) à travers différents flux, un flux étant un intermédiaire entre une source de données et sa destination. Il va par exemple être possible d'échanger des données avec un fichier, avec la mémoire... 

Il existe en particulier deux types de flux : d'une part les flux d'octets, d'autre part les flux de caractères. Les classes abstraites utilisées pour gérer les flux diffèrent donc selon leur type. Pour les flux d'octets, on utilisera les classes \texttt{InputStream} et \texttt{OutputStream}, alors que \texttt{Reader} et \texttt{Writer} serviront pour les flux de caractères.

On peut ensuite distinguer des sous-classes de ces différentes classes. On peut donc trouver :
\begin{itemize}[label=\textbullet, font=\large]
\item Les sous-classes \texttt{File*} : elles permettent d'interagir avec des fichiers.
\item Les sous-classes \texttt{Buffered*} : elles servent à manipuler un ensemble d'octets/caractères plutôt que de les traiter un à un.
\item Les sous-classes \texttt{Data*} : elles font les conversions entre objets et octets (pour stocker dans un fichier binaire par exemple).
\end{itemize}

Il existe aussi la classe \texttt{Scanner} qui permet, à partir d'un flux, de récupérer les éléments voulus.

\subsection{Code commenté}

Afin de présenter un exemple concret d'utilisation, nous avons implémenté un programme illustrant les différents types de flux d'Entrées/Sorties.

\inputminted[fontsize=\footnotesize]{java}{TestEntreeSortie.java}

Nous allons maintenant expliciter quelques passages de ce code. \\
D'une part, nous allons étudier les lignes qui concernent directement les flux. 
\begin{minted}{Java}
dos = new DataOutputStream(new FileOutputStream("fichierbinaire"));
\end{minted}
Cette ligne permet d'ouvrir en écriture un fichier binaire nommé \og fichierbinaire \fg{}. Si celui-ci n'existe pas dans le répertoire où se trouve le fichier \texttt{TestEntreeSortie.java}, il y est créé. Dans ce passage, le constructeur \texttt{FileOutputStream} permet de créer une instance de la classe du même nom. Les éléments de cette classe permet d'accéder au contenu du fichier binaire pour y écrire. Cependant, cette instance ne manipule que des octets, pas des objets. C'est la raison pour laquelle on utilise également une instance de la classe \texttt{DataOutputStream}, en donnant l'objet de \texttt{FileOutputStream} en paramètre du constructeur. Cela va permettre de convertir les objets en octets lors de l'écriture dans le fichier. On notera que les classes utilisées héritent ici de \texttt{OutputStream}, car ce sont des flux d'octets qui sont manipulés.
\begin{minted}{Java}
FileWriter fileWriter = new FileWriter("fichiertexte");
\end{minted}
Cette autre ligne de code permet d'ouvrir en écriture le fichier texte nommé \og fichiertexte \fg{}. Comme précédemment, celui-ci est créé s'il n'existe pas encore. Cette fois, il n'y a pas besoin de faire des conversions entre objet et octet, car ce qu'on écrira sera déjà du texte. On utilisera donc uniquement une instance de la classe \texttt{FileWriter}. C'est une sous-classe de \texttt{Writer} car il s'agit ici de gérer des flux de caractères.
\begin{minted}[fontsize=\footnotesize]{Java}
DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream("fichierbinaire")));
\end{minted}
Cette fois, ce code ouvre en lecture le fichier binaire dans lequel un entier et un double ont été écrits précédemment. Détaillons encore une fois les différentes classes utilisées pour y parvenir. Nous pouvons déjà remarquer, comme ce sont des octets qui sont utilisés dans ce fichier, toutes ces classes dernières héritent de \texttt{InputStream}. Tout d'abord, l'objet de classe \texttt{FileInputStream} permet d'accéder au contenu de \og fichierbinaire \fg{} pour le lire. On trouve ensuite \texttt{BufferedInputStream}, dont l'utilisation permet de traiter plusieurs octets à la fois, plutôt que de le faire un par un. Finalement, l'instance de la classe \texttt{DataInputStream}, qui prend l'objet issu de \texttt{BufferedInputStream} en entrée de son constructeur, fera la conversion d'octets en objets, afin de retrouver une configuration permettant un traitement par objet de ces données.
\begin{minted}{Java}
BufferedReader fileReader = new BufferedReader(new FileReader("fichiertexte"));
\end{minted}
Enfin, étudions la ligne qui ouvre en lecture le fichier texte vu précédemment. Encore une fois, la manipulation de caractères impose que les classes utilisées soient des sous-classes de \texttt{Reader}. On a alors une instance de \texttt{FileReader}, permettant de lire le contenu du fichier \texttt{fichiertexte}, qui est en paramètre du constructeur d'une instance de la classe \texttt{BufferedReader}. L'utilisation de cette dernière classe permet de lire une chaine de caractères entière plutôt que de le faire caractère par caractère.
\\~\\
De plus, pour fermer chacun de ces fichiers en lecture ou écriture, nous appliquons la méthode \texttt{close()} correspondant à l'instance utilisée.\\

D'autre part, nous avons géré toutes les exceptions renvoyées par l'utilisation de toutes ces classes. Nous n'avons pas fait de traitement particulier en fonction de l'exception relevée, mais nous affichons tout de même à l'utilisateur qu'il y a eu une erreur, ainsi que le type de cette dernière.


\subsection{Exemple d'utilisation}

Dans cet exemple, différents flux sont utilisés : les flux standards, les flux vers des fichiers binaires ou textes et les flux par les arguments.
Il est important de noter que l'utilisation de l'entrée par les arguments implique qu'il est nécessaire de donner un argument lors de l'appel du programme pour son bon fonctionnement. Il doit être de la forme \og 67-42,6\fg{} pour permettre le traitement prévu. 
Puis, lorsque le programme est lancé, on commence par utiliser la lecture par le flux standard (entré par l'utilisateur dans le terminal) et l'écriture dans un fichier. Il est donc demandé à l'utilisateur d'entrer un entier, un réel, et enfin une chaîne de caractères (cf figure \ref{utilisation_entrees-sorties}). Les deux premiers seront alors écrits dans le fichier \texttt{fichierbinaire} à l'aide de flux binaires, alors que la chaîne de caractères est écrite dans le fichier texte \texttt{fichiertexte} à l'aide de flux de caractères. 


C'est ensuite le processus inverse qui est mis en oeuvre. Le programme commence par rechercher les informations dans les fichiers avant de les afficher sur le terminal, en utilisant ainsi le flux de sortie standard.

Finalement, les arguments passés lors l'appel du programme sont traités. Il se base sur la structure de l'argument pour en extraire un entier et un réel, puis les affiche sur le terminal.

\begin{figure}[!ht]
\begin{center}
\includegraphics[scale=0.5]{images/utilisation_entrees-sorties.png}\\
\caption{Capture d'écran d'un exemple de fonctionnement du programme}
\label{utilisation_entrees-sorties}
\end{center}
\end{figure}

\newpage
\section{La sérialisation}

\subsection{Présentation générale}

La sérialisation est un procédé qui permet d'envoyer des objets à travers des flux de sortie comme ceux vus dans la partie précédente. En particulier, on l'utilise notamment pour pouvoir stocker des objets dans des fichiers, ce qui permet de sauvegarder nos objets. On appelle désérialisation le procédé inverse qui permet de reconstituer un objet à partir d'un flux d'entrée.

En Java, les flux utilisés sont alors \texttt{ObjectInputStream} et \texttt{ObjectOutputStream}. On utilisera principalement leurs méthodes respectives \texttt{readObject} et \texttt{writeObject}, mais on peut également écrire des types primitifs, comme par exemple avec les méthodes \texttt{writeInt} et \texttt{readInt}.

Dans la version classique de la sérialisation que nous allons voir pour commencer, il suffit que l'objet à sérialiser implémente l'interface \texttt{Serializable}. On pourra alors simplement le sérialiser en instanciant un \texttt{ObjectOutputStream} et en utilisant sa méthode \texttt{writeObject}. Tous les attributs de l'objet seront alors sérialisés, sauf les attributs qui ont été associés au mot-clé \mintinline{Java}{transient}.

On verra également qu'on peut personnaliser le comportement par défaut de la sérialisation d'une première façon, en redéfinissant les méthodes \texttt{readObject} et \texttt{writeObject} dans la classe à sérialiser.

Cette méthode classique de sérialisation stocke, en plus de l'objet, des méta-données qui lui sont liées, ce qui prend plus de temps. On peut contrôler totalement les étapes de la sérialisation en implémentant une autre interface que \texttt{Serializable}, qui est \texttt{Externalizable}. La sérialisation est alors bien plus rapide, cependant elle oblige à redéfinir les méthodes d'écriture et de lecture : cette méthode est donc moins ``clé en main'' que la façon classique. De plus, elle est moins sécurisée car les méthodes de lecture et d'écriture deviennent publiques, et donc utilisables par n'importe quelle autre classe, alors qu'elles étaient privées dans la méthode précédente. Remarque supplémentaire, contrairement à la méthode classique, la sérialisation avec \texttt{Externalizable} utilise le constructeur de la classe lors de la désérialisation.

On verra également la sérialisation par objet proxy : cette méthode permet, lors de la sérialisation, de stocker un objet autre que l'objet initial, qui est donc appelé objet proxy. De plus, on peut également instancier un objet différent de l'objet réellement stocker lors de la désérialisation. Cela peut permettre par exemple, lorsqu'on a créé une nouvelle classe, et qu'on possède encore des objets d'une ancienne classe qui n'est plus utilisée, de pouvoir continuer à utiliser les objets stockés mais en créant des instances de la nouvelle classe.

\subsection{Code commenté}

On testera les différentes méthodes évoquées dans une classe \texttt{TestSerialisation}. On mesurera le temps que met chaque méthode à effectuer la sérialisation puis la désérialisation dans un fichier. Voici la classe que l'on cherche à sérialiser :

\begin{figure}[!ht]
\begin{center}
\includegraphics[scale=0.75]{images/diagramme_classe_personne.png}\\
\caption{Représentation UML de la classe \texttt{Personne}}
\label{diagramme_classe_personne}
\end{center}
\end{figure}

Pour toutes les méthodes, on ne sérialisera pas l'attribut \texttt{adresse}.

\subsubsection{Sérialisation classique}

Pour la première méthode, on fait l'implémentation suivante de \texttt{Personne} :

\inputminted[lastline=51]{java}{Personne.java}

On voudrait que l'adresse ne soit pas stockée, on a donc défini cet attribut comme \mintinline{Java}{transient}.

Le test se déroule donc de la façon suivante :

\inputminted[lastline=44, fontsize=\footnotesize]{java}{TestSerialisation.java}

Les appels à la méthode \mintinline{Java}{System.currentTimeMillis()} permet de mesurer le temps mis par la sérialisation et désérialisation. On commence par instancier un objet de la classe \texttt{Personne}, puis un \texttt{ObjectOutputStream}. On appelle le constructeur de \texttt{ObjectOutputStream} en lui passant un \texttt{FileOutputStream}, ce qui nous permet d'ouvrir un fichier en écriture. Ensuite, on sérialise avec \texttt{writeObject}, puis on appelle la méthode \texttt{flush} pour sauvegarder les modifications.

De même, pour la lecture, on instancie un \texttt{ObjectInputStream}, puis on stocke dans un nouvel objet le résultat de la méthode \texttt{readObject}. On affiche l'objet avant et après sérialisation, et on affiche également le temps que cela a mis.

Lors de la sérialisation et de la désérialisation, on doit gérer les exceptions qui peuvent être causées, notamment si l'on ne parvient pas à ouvrir le fichier, ou si une erreur survient lors du processus. On doit également gérer lors de la désérialisation l'exception \texttt{ClassNotFoundException} qui peut être levée si le programme ne parvient pas à retrouver la classe demandée.

On va également faire la personnalisation des méthodes de sérialisation comme on l'a évoqué dans la présentation.

\inputminted[firstline=53, lastline=65, fontsize=\footnotesize]{java}{Personne.java}

Ici, on rajoute simplement l'affichage d'un message lors de la sérialisation et de la désérialisation, mais on peut imaginer n'importe quel traitement, comme chiffrer un mot de passe avant de le stocker par exemple.  

\subsubsection{Sérialisation avec \texttt{Externalizable}}

On fait une deuxième version de la classe \texttt{Personne}, où on implémente l'interface \texttt{Externalizable}.

\inputminted[fontsize=\small]{java}{PersonneExternalizable.java}

Les seules différences sont que l'on implémente \texttt{Externalizable} à la place de \texttt{Serializable}, et que l'on redéfinit cette fois les méthodes \texttt{writeExternal} et \texttt{readExternal}. Nous sommes obligés de les redéfinir car elles font partie de l'interface, ce qui nous force donc à personnaliser notre sérialisation. Ces méthodes sont \mintinline{java}{public} et donc accessibles de n'importe où. Nous verrons dans la partie suivante l'efficacité de cette méthode.

\subsubsection{Sérialisation par objet proxy}

Nous définissons pour cette méthode deux nouvelles classes : \texttt{PersonneARemplacer}, qui est quasiment une copie de \texttt{Personne} mais adaptée à notre méthode, et \texttt{PersonneProxy} qui sera l'objet réellement sérialisé et qui remplacera \texttt{PersonneARemplacer}. Puisqu'on a peu de chose à modifier dans \texttt{PersonneARemplacer}, on la fait hériter de \texttt{Personne}. Voici son implémentation :

\inputminted{java}{PersonneARemplacer.java}

Dans cette classe, on doit simplement ajouter la méthode \texttt{writeReplace}, qui va créer une \texttt{PersonneProxy} à partir de l'objet courant lors de la sérialisation.

On écrit donc également la classe \texttt{PersonneProxy} :

\inputminted[fontsize=\footnotesize]{java}{PersonneProxy.java}

Nous n'avons qu'un seul champ dans cette classe, qui contient toutes les informations sous forme de chaîne de caractères.
On a ensuite le constructeur qui créé un objet à partir de \texttt{PersonneARemplacer}, que l'on a utilisé juste au-dessus. Ce dernier met donc les attributs dans une chaîne. La méthode \texttt{readResolve} permet donc de créer une \texttt{PersonneARemplacer} lors de la désérialisation à partir des informations de la chaîne. 

\subsection{Exemples d'utilisation}
\newpage

On va maintenant exécuter notre programme de test des différentes méthodes de sérialisation.

\begin{figure}[!ht]
\begin{center}
\includegraphics[scale=0.6]{images/utilisation_serialisation.png}\\
\caption{Capture d'écran du fonctionnement du programme}
\label{utilisation_serialisation}
\end{center}
\end{figure}

On fait tout d'abord la sérialisation classique. On affiche les informations contenues dans notre objet, puis on le sérialise et on le désérialise. On peut s'apercevoir que l'adresse, qui était \mintinline{java}{transient}, n'a pas été stockée lors de la sérialisation. On remarque également que les messages que l'on a affiché grâce à la personnalisation sont bien présents. Le temps est sur cet exemple de 165 millisecondes.

On étudie ensuite la sérialisation avec \texttt{Externalizable}. Lors de la désérialisation, le constructeur par défaut de l'objet est bien appelé. Comme on l'a voulu, l'adresse n'a pas été stockée. On remarque principalement que le temps est bien plus faible (environ 1 milliseconde), et donc cette sérialisation est largement plus rapide que la précédente.

Enfin, on effectue la sérialisation par objet proxy. Elle s'effectue sans problème, et une fois de plus l'adresse n'a pas été conservée. On s'aperçoit que cette méthode met un temps comparable à celui de la méthode précédente, ce qui est logique finalement car comme pour la sérialisation avec \texttt{Externalizable}, nous devons définir nous-mêmes ce que fait la sérialisation, et il n'y a donc pas de stockage de méta-données.

Lorsqu'on ouvre le fichier \texttt{fichierobjetproxy}, bien entendu il n'est pas lisible convenablement car ce n'est pas un fichier texte, mais on peut distinguer que c'est bien un objet proxy qui a été stocké.\\

\begin{figure}[!ht]
\begin{center}
\includegraphics[scale=0.5]{images/fichierobjetproxy.png}\\
\caption{Fichier \texttt{fichierobjetproxy}}
\label{fichierobjetproxy}
\end{center}
\end{figure}

\section*{Conclusion}
\addcontentsline{toc}{section}{Conclusion}

Ce projet montre finalement que les Entrées/Sorties et la sérialisation sont des outils de Java faciles d'utilisation et qui ont de nombreuses fonctionnalités. D'une part, les Entrées/Sorties permettent d'écrire ou de lire des données extérieures au programme, dans un fichier, dans les informations transmises par l'utilisateur par l'intermédiaire du clavier, ou dans celles qui lui sont affichées sur le terminal par exemple. Cet outil sert donc d'intermédiaire entre le programme et son environnement. D'autre part, la sérialisation permet de stocker durablement un objet dans un état particulier, et de le récupérer plus tard. Ces outils peuvent donc être d'une grande utilité dans un grand nombre d'applications développées sous Java.

La réalisation de ce projet a donc été l'occasion pour nous d'en apprendre plus sur ces fonctionnalités de Java, et de nous familiariser avec ce langage de programmation.

\end{document}
